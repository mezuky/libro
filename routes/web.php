<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::put('books/{book}/borrow', 'BooksController@borrow')->name('books.borrow');
Route::put('books/{book}/return', 'BooksController@return')->name('books.return');
Route::get('books/borrowed', 'BooksController@borrowed')->name('books.borrowed');
Route::get('books/available', 'BooksController@available')->name('books.available');
Route::resource('books', 'BooksController');

Route::get('authors', 'AuthorsController@index')->name('authors.index');
Route::get('authors/{author}', 'AuthorsController@show')->name('authors.show');
