<?php

use Illuminate\Database\Seeder;
use App\Author;
use App\Book;

class BooksTableSeeder extends Seeder
{
    protected $books = 20;

    protected $authors = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 1; $i <= $this->books; $i++) {

            $author = $this->author();

            for ($j = 1; $j <= $this->authors; $j++) {
                $book = new Book();
                $book->title = $faker->sentence(3);
                $book->isbn = $faker->isbn13;
                $book->author_id = $author->id;
                $book->borrowed = rand(0,1);
                $book->save();
            }
        }
    }

    protected function author()
    {
        return factory(Author::class)->create();
    }
}
