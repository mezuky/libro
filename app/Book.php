<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Author;

class Book extends Model
{
    protected $fillable = [
        'title',
        'isbn',
        'author_id',
    ];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function scopeBorrowed($query)
    {
        return $query->where('borrowed', true);
    }

    public function scopeAvailable($query)
    {
        return $query->where('borrowed', false);
    }
}
