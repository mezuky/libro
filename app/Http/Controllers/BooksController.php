<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Http\Requests\Book\Store;
use App\Author;

class BooksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('title', 'asc')->get();
        return view('books.index', compact('books'));
    }

    public function borrowed()
    {
        $books = Book::orderBy('title', 'asc')->borrowed()->get();
        return view('books.index', compact('books'));
    }

    public function available()
    {
        $books = Book::orderBy('title', 'asc')->available()->get();
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $author = Author::firstOrCreate(['name' => $request->author]);
        $book = new Book();
        $book->fill($request->only(['title', 'isbn']));
        $book->author_id = $author->id;
        $book->save();

        return redirect()->route('books.index')->with('success', 'Cartea a fost adăugată.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('books.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index')->with('success', 'Cartea a fost ștearsă.');
    }

    public function borrow(Book $book)
    {
        $book->borrowed = true;
        $book->save();
        return redirect()->back()->with('success', 'Cartea a fost împrumutată.');
    }

    public function return(Book $book)
    {
        $book->borrowed = false;
        $book->save();
        return redirect()->back()->with('success', 'Cartea a fost returnată.');
    }
}
