@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            @if(count($books) > 0)
                <div class="panel panel-default">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Titlu</th>
                                <th>Autor</th>
                                <th>ISBN</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($books as $book)
                                <tr>
                                    <td><a href="{{ route('books.show', ['book' => $book->id]) }}">{{ $book->title }}</a></td>
                                    <td><a href="{{ route('authors.show', ['author' => $book->author->id]) }}">{{ $book->author->name }}</a></td>
                                    <td>{{ $book->isbn }}</td>
                                    <td>
                                        @if($book->borrowed)
                                            <span class="label label-danger">Împrumutată</span>
                                        @else
                                            <span class="label label-success">Pe raft</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-warning">Nu există nicio carte.</div>
            @endif
        </div>
    </div>
</div>
@endsection
