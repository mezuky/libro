@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Titlu: </strong>{{ $book->title }}</div>
                <div class="panel-body"><strong>Autor: </strong>{{ $book->author->name }}</div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            @if (!$book->borrowed)
                                {!! Form::open(['route' => ['books.borrow', $book->id], 'method' => 'put']) !!}
                                    {!! Form::submit('Împrumută cartea', ['class' => 'btn btn-block btn-success']) !!}
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['route' => ['books.return', $book->id], 'method' => 'put']) !!}
                                    {!! Form::submit('Returnează cartea', ['class' => 'btn btn-block btn-info']) !!}
                                {!! Form::close() !!}
                            @endif
                        </div>
                        <div class="col-xs-6">
                            {!! Form::open(['route' => ['books.destroy', $book->id], 'method' => 'delete']) !!}
                            {!! Form::submit('Șterge cartea', ['class' => 'btn btn-block btn-danger']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
