@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['route' => 'books.store']) !!}
                        <div class="form-group">
                            {!! Form::label('title', 'Titlu:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Steve Jobs - Biografia autorizată']) !!}
                            @if ($errors->has('title'))
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('isbn', 'ISBN:') !!}
                            {!! Form::text('isbn', null, ['class' => 'form-control', 'placeholder' => '9789731931937']) !!}
                            @if ($errors->has('isbn'))
                                <span class="text-danger">{{ $errors->first('isbn') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('author', 'Autor:') !!}
                            {!! Form::text('author', null, ['class' => 'form-control', 'placeholder' => 'Walter Isaacson']) !!}
                            @if ($errors->has('author'))
                                <span class="text-danger">{{ $errors->first('author') }}</span>
                            @endif
                        </div>
                        {!! Form::submit('Adaugă cartea', ['class' => 'btn btn-block btn-info']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
