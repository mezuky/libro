@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Despre proiect</div>
                <div class="panel-body">
                    <strong>Autor: </strong>Alexandru Bugărin
                    <br>
                    <strong>Contact: </strong><a href="mailto:alexandru.bugarin@gmail.com">alexandru.bugarin@gmail.com</a>
                    <br>
                    <strong>Codul sursă: </strong><a target="_blank" href="https://gitlab.com/mezuky/libro">Libro</a>
                    <br>
                    <strong>Descriere: </strong>Proiectul a fost creat pentru cursul <strong>Algoritmi și Structuri de Date 2</strong>. Libro este o aplicație pentru gestionarea cărților dintr-o bibliotecă.
                    <br>
                    <strong>Funcții disponibile:</strong>
                    <ul>
                        <li>Crearea unui cont</li>
                        <li>Accesarea aplicației cu contul creat</li>
                        <li>Afșiarea tuturor cărților (titul, autor, isbn, status)</li>
                        <li>Afișarea tuturor cărților pe raft (titlu, autor, isbn)</li>
                        <li>Afișarea tuturor cărților împrumutate (titlu, autor, isbn)</li>
                        <li>Afișarea cărților unui autor</li>
                        <li>Adăugare carte</li>
                        <li>Împrumutare carte</li>
                        <li>Returnare carte</li>
                        <li>Ștergere carte</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
